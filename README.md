## About me
- :mortar_board:         Third year Computer Science student at Poznan University of Technology
- :battery: &nbsp;       Full of ideas and always looking for ways to share them. This GitLab profile is exactly for that!
- :card_file_box:        Interested especially in `Low-level programming` `Web technology` `Artificial intelligence` `Game programming`
- :ox:                   I love tinkering, customizing and exploring. That's why I'm a [Free Software](https://www.gnu.org/philosophy/free-sw.html) enthusiast!

## Some of my favorite and most used tools

<a href="https://www.gnu.org/software/emacs/">
    <img alt="Emacs" src="https://www.gnu.org/savannah-checkouts/gnu/emacs/images/emacs.png" width="128" height="128">
</a>
&nbsp;
<a href="https://sw.kovidgoyal.net/kitty/">
    <img alt="Kitty" src="https://sw.kovidgoyal.net/kitty/_static/kitty.svg" width="128" height="128">
</a>
&nbsp;
<a href="https://gcc.gnu.org">
    <img alt="GCC" src="https://upload.wikimedia.org/wikipedia/commons/a/af/GNU_Compiler_Collection_logo.svg" width="128" height="128">
</a>
&nbsp;
<a href="https://wordpress.org">
    <img alt="Wordpress" src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F9%2F98%2FWordPress_blue_logo.svg%2F480px-WordPress_blue_logo.svg.png&f=1&nofb=1" width="128" height="128">
</a>

## Preferred programming languages

<a href="https://wikipedia.org/wiki/C%2B%2B">
    <img alt="C++" src="https://upload.wikimedia.org/wikipedia/commons/1/18/ISO_C%2B%2B_Logo.svg" width="113" height="128">
</a>
&nbsp;
<a href="https://developer.mozilla.org/docs/Learn/JavaScript/First_steps/What_is_JavaScript">
    <img alt="JavaScript" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1024px-Unofficial_JavaScript_logo_2.svg.png" width="108" height="108">
</a>
&nbsp;
<a href="https://www.lua.org">
    <img alt="Lua" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Lua-Logo.svg/1024px-Lua-Logo.svg.png" width="128" height="128">
</a>
